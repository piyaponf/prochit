package com.porch.it

import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class ActivityPage7 : AppCompatActivity() {

    lateinit var username: EditText
    lateinit var password: EditText

    private val URL_ROOT = "http://localhost/phpmyadmin/tbl_structure.php?db=farmers&table=user"
    val URL_LOGIN = URL_ROOT + "Login"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page7)

        username = findViewById(R.id.username)
        password = findViewById(R.id.password)

        username.setOnClickListener {
        }

    }
}
