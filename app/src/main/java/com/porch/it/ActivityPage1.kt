package com.porch.it

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class ActivityPage1 : AppCompatActivity() {

    private val im10: ImageView by lazy { findViewById(R.id.im10) }
    private val im11: ImageView by lazy { findViewById(R.id.im11) }
    private val im12: ImageView by lazy { findViewById(R.id.im12) }
    private val im18: ImageView by lazy { findViewById(R.id.im18) }
    private val im19: ImageView by lazy { findViewById(R.id.im19) }
    private val im20: ImageView by lazy { findViewById(R.id.im20) }
    private val im21: ImageView by lazy { findViewById(R.id.im21) }
    private val back: ImageView by lazy { findViewById(R.id.back) }
    private val home: ImageView by lazy { findViewById(R.id.home) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page1)

        im10.setOnClickListener {
            val i = Intent(this, ActivityPage2::class.java)
            startActivity(i)
        }
        im11.setOnClickListener {
            val i = Intent(this, ActivityPage5::class.java)
            startActivity(i)
        }
        im12.setOnClickListener {
            val i = Intent(this, ActivityPage6::class.java)
            startActivity(i)
        }
        im18.setOnClickListener {
            val i = Intent(this, ActivityPage12::class.java)
            startActivity(i)
        }
        im19.setOnClickListener {
            val i = Intent(this, ActivityPage15::class.java)
            startActivity(i)
        }
        im20.setOnClickListener {
            val i = Intent(this, ActivityPage13::class.java)
            startActivity(i)
        }
        im21.setOnClickListener {
            val i = Intent(this, ActivityPage14::class.java)
            startActivity(i)
        }
        back.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
        }
        home.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
        }
    }
}
