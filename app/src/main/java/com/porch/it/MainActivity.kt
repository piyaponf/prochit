package com.porch.it

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var st: ImageView
    private lateinit var ad: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        st = findViewById(R.id.st)
        ad = findViewById(R.id.ad)


        st.setOnClickListener {
            val i = Intent(this, ActivityPage1::class.java)
            startActivity(i)
        }
        ad.setOnClickListener {
            val i = Intent(this, ActivityPage7::class.java)
            startActivity(i)
        }

    }
}
