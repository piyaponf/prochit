package com.porch.it

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class ActivityPage9 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page9)

        val spinner1: Spinner = findViewById(R.id.spinner1)

        val activitiesList = arrayListOf("หินปูน", "โดโลไมท์", "ปูนขาว")
        ArrayAdapter(this, android.R.layout.simple_spinner_item, activitiesList).also {
            it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner1.adapter = it
        }
        spinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Toast.makeText(
                    this@ActivityPage9,
                    "You selected : ${activitiesList[p2]}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
