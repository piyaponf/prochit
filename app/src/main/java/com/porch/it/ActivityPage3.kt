package com.porch.it

import android.os.Bundle
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener

class ActivityPage3 : AppCompatActivity() {

    private val rdg: RadioGroup by lazy { findViewById(R.id.rbg1) }
    private val nv3: EditText by lazy { findViewById(R.id.num3) }
    private val nv4: EditText by lazy { findViewById(R.id.num4) }
    private val nv5: EditText by lazy { findViewById(R.id.num5) }
    private val sb1: SeekBar by lazy { findViewById(R.id.seekBar1) }
    private val sb1Text: EditText by lazy { findViewById(R.id.editTextNumber9) }
    private val sb2: SeekBar by lazy { findViewById(R.id.seekBar2) }
    private val sb3: SeekBar by lazy { findViewById(R.id.seekBar3) }
    private val sb4: SeekBar by lazy { findViewById(R.id.seekBar4) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_3)


        val num1 = intent.getIntExtra("num1", 0)
        // val num2 = intent.getIntExtra("num2", 0)

        nv3.setText((num1 * 1.02).toString())
        nv4.setText((num1 * 0.4).toString())
        nv5.setText((num1 * 2.5).toString())

        sb1.max = 100
        sb1.progress = 50

        sb1Text.addTextChangedListener({ _, _, _, _ ->
        }, { charSequence, _, _, _ ->
            val pgs = charSequence.toString().toDouble()
            if (pgs < 10.0 && pgs >= 0.0) {
                val percent = pgs * 100 / 10.0
                sb1.progress = percent.toInt()
            }
        }, { _ ->
        })

        sb1.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seek: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    val pgs = 10.0 * progress / 100
                    sb1Text.setText(pgs.toString())
                }
            }

            override fun onStartTrackingTouch(seek: SeekBar?) {
            }

            override fun onStopTrackingTouch(seek: SeekBar?) {
            }
        })

    }
}
