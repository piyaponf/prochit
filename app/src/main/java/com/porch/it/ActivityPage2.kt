package com.porch.it

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class ActivityPage2 : AppCompatActivity() {

    private val bt7: Button by lazy { findViewById(R.id.bt7) }
    private val back: ImageView by lazy { findViewById(R.id.back) }
    private val num1: EditText by lazy { findViewById(R.id.num1) }
    private val num2: EditText by lazy { findViewById(R.id.num2) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_2)

        bt7.setOnClickListener {
            Intent(this, ActivityPage3::class.java).also {
                startActivity(it)
            }
        }

        back.setOnClickListener {
            Intent(this, ActivityPage1::class.java).also {
                startActivity(it)
            }
        }

        Intent(this, ActivityPage3::class.java).also {
            it.putExtra("num1", num1.text.toString().toInt())
            it.putExtra("num2", num2.text.toString().toInt())
            startActivity(it)
        }
    }
}
